FROM python:3.7.6-slim

ENV APP_PATH /usr/src/app
WORKDIR $APP_PATH

RUN apt-get -qq update && apt-get -qq install build-essential -y && apt-get -qq install unixodbc -y && \
    apt-get -qq install unixodbc-dev -y && apt-get -qq install libpq-dev -y \
    && apt-get -qq install python3-dev -y


COPY . $APP_PATH

RUN pip install --upgrade pip 
RUN pip install -r requirements.txt

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput

#EXPOSE 8000


# && apt-get unixodbc && apt-get unixodbc-dev && \
#     apt-get libpq-dev && apt-get python3-dev